#!/usr/bin/env bash

echo "SFTP Auto is a script that will create a Debian SFTP Server"

clear

sleep 2

sftpgroup=$1

#adding sftp group
function group_add {

    if [[ -z $sftpgroup ]]; then
	echo "You need to type a SFTP group name just after ./install_sftp!" \
	&& exit 1
    else
	addgroup $sftpgroup
    fi
}


#configuring sshd file for sftp access
function edit_sshd { 
    echo -e  "Match Group "$sftpgroup"\n    X11Forwarding no\n    AllowTcpForwarding no\n    ChrootDirectory %h\n    ForceCommand internal-sftp">>/etc/ssh/sshd_config

}

#conditional to verify if user is root, since we need to edit under /etc this is required
needroot=$(whoami)
if [[ $needroot == "root" ]]
then
    group_add && edit_sshd
    #systemctl restart sshd
else
    echo "You need to be root." \
    && exit 1
fi

