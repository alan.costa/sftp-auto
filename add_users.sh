#!/usr/bin/env bash

echo "Looking for the users list file..."

clear

sleep 2

function user_grp_creation {
    
    if [[ `ls -l | grep -o user_list.txt` ]];
    then

	#take the sftp group name field on sshd_config file and assign to variable $sftp_grp
	#this is done so we can add users to the correct sftp group
	sftp_grp=$(cat /etc/ssh/sshd_config | grep Match\ Group | cut -d ' ' -f3)
	user_list=$(cat user_list.txt) #list of users to be added to sftp
	for i in $user_list;
	do

	    useradd -m $i -G $sftp_grp
	    chmod 755 /home/$i
	    chown root:root /home/$i
            cd /home/$i \
		&& mkdir arquivos \
		&& chmod 700 arquivos \
		&& chown $i:$i arquivos

	done
    else
	echo "Please, we need a user_list.txt file with all users you need to add." \
	     && exit 1
    fi

}

needroot=$(whoami)

if [[ $needroot == "root" ]];

then
    user_grp_creation
else
    echo "You need to be root." \
	 && exit 1
fi




	
	
